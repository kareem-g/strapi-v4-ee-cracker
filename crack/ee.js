"use strict";

const _ = require("lodash");

const noop = () => {};

const noLog = {
  warn: noop,
  info: noop,
};

const internals = {};
const features = {
  bronze: [],
  silver: [],
  gold: ["sso"],
};

module.exports = ({ logger = noLog }) => {
  if (_.has(internals, "isEE")) return internals.isEE;

  // Credits to RootIsBack (Fuggedaboutit.)
  logger.warn(`

  █▀█ █▀█ █▀█ ▀█▀ █ █▀ █▄▄ ▄▀█ █▀▀ █▄▀
  █▀▄ █▄█ █▄█ ░█░ █ ▄█ █▄█ █▀█ █▄▄ █░█

  Fuggedaboutit.
  ـــــــــــــــــــــــــــــــــــــــــــــ
`);

  internals.licenseInfo = {
    // We Don't need the expireAt because we don't want to expire it - hahaha - really! it just here for Fun :)
    expireAt: "2099-12-31",
    type: "gold",
  };

  internals.isEE = true;
  // Log License Info to The console :D
  logger.info(
    "Your Plan Type is: " +
      JSON.stringify(internals.licenseInfo.type) +
      " and it will expire in " +
      JSON.stringify(internals.licenseInfo.expireAt)
  );

  return true;
};

Object.defineProperty(module.exports, "licenseInfo", {
  get() {
    return internals.licenseInfo;
  },
  configurable: false,
  enumerable: false,
});

Object.defineProperty(module.exports, "isEE", {
  get() {
    return internals.isEE;
  },
  configurable: false,
  enumerable: false,
});

Object.defineProperty(module.exports, "features", {
  get() {
    const { type: licenseType } = module.exports.licenseInfo;

    return {
      isEnabled(feature) {
        return features[licenseType].includes(feature);
      },
      getEnabled() {
        return features[licenseType];
      },
    };
  },
  configurable: false,
  enumerable: false,
});
