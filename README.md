# intalling the Package
```
# yarn
yarn add kareem-ee

or

# npm
npm install kareem-ee
```

# Usage 
## Add The following script to your `package.json`

```
  "scripts": {
     "crack": "strapi-crack"
  },
```

## Then run `yarn run crack` or `npm run crack`

# Please Note: When you install a new package or update your `node_modules` crack will get removed so you need to run it again..
## i'll fix this issue in the future

> RootIsBack
